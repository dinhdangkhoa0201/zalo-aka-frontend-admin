import React, {useState} from "react"
import {
    Box,
    Button,
    Checkbox,
    Divider,
    FormControl,
    FormControlLabel,
    Grid,
    Link, Slide,
    Stack,
    TextField,
    Typography,
    IconButton, Modal, CircularProgress
} from "@mui/material";
import {Facebook, Google} from "@mui/icons-material";
import {WelcomeSvgComponent} from "../../images/component/WelcomeSvgComponent";
import {red} from "@mui/material/colors";
import LoadingButton from "@mui/lab/LoadingButton";

const color = red[500];

export function Login() {
    /* START: Variable */
    const [isLoading, setIsLoading] = useState<boolean>(false);
    /* END: Variable */

    /* START: Function */
    const handleLogin = () => {
        setIsLoading(true);
    }
    /* END: Function */


    return (
        <Grid container
              justifyContent={"center"}
              alignItems={"center"}
              p={"25px"}
              height={"100vh"}>
            <Slide direction="right" in={true}>
                <Stack direction={"row"} width={"100%"}>
                    <Stack boxShadow={1}
                           borderRadius={5}
                           alignItems={"center"}
                           justifyContent={"center"}
                           width={"50%"}>
                        <Typography variant={"h3"} fontWeight={"500"}>
                            Hi, Welcome Back!
                        </Typography>
                        <WelcomeSvgComponent/>
                    </Stack>
                    <Stack width={"50%"}
                           alignItems={"center"}
                           justifyContent={"center"}
                           p={"50px"}>
                        <Stack spacing={3} width={"50%"}>
                            {/* START: Title */}
                            <Typography variant={"h3"} mb={2} fontWeight={"500"}>
                                Sign in to Zalo Aka
                            </Typography>
                            {/* END: Title */}

                            {/* START: Login With Social Network */}
                            <Stack direction="row" spacing={2} justifyContent={"space-between"}>
                                <Button variant={"outlined"}
                                        sx={{padding: 1.5}}
                                        fullWidth
                                        size={"large"}
                                        startIcon={<Facebook sx={{margin: 0}} fontSize={"large"} color={"primary"}/>}/>
                                <Button variant={"outlined"}
                                        fullWidth
                                        size={"large"}
                                        startIcon={<Google sx={{margin: 0}} fontSize={"large"} htmlColor={"#f00"}/>}/>
                            </Stack>
                            {/* END: Login With Social Network */}

                            <Divider variant="middle"/>

                            {/* START: Field Username - Password */}
                            <Box>
                                <Stack spacing={3}>
                                    <TextField label={"Email or Username"}/>
                                    <TextField label="Password"
                                               type="password"/>
                                </Stack>
                            </Box>
                            {/* END: Field Username - Password */}

                            {/* START: Remember me - Forgot password */}
                            <Stack direction={"row"}
                                   alignItems={"center"}
                                   justifyContent={"space-between"}>
                                <FormControl>
                                    <FormControlLabel control={<Checkbox/>} label={"Remember me"}/>
                                </FormControl>
                                <Typography variant={"body1"} fontWeight={"500"}>
                                    <Link href={"/resetPassword"} underline={"hover"}>Forget password?</Link>
                                </Typography>
                            </Stack>
                            {/* END: Remember me - Forgot password */}

                            {/* START: Login Button */}
                            <Stack direction="row" spacing={2}>
                                <LoadingButton variant={"contained"}
                                               loading={isLoading}
                                               sx={{padding: 2, borderRadius: 3}}
                                               fullWidth
                                               size={"large"}
                                               onClick={() => handleLogin()}>Log in</LoadingButton>
                            </Stack>
                            {/* END: Login Button */}

                            {/* START: Register */}
                            <Stack direction="row">
                                <Typography variant={"body1"}>
                                    Don't have an account? <Link href={"/register"} underline={"hover"} fontWeight={"500"}>Sign up</Link>
                                </Typography>
                            </Stack>
                            {/* END: Register */}

                        </Stack>
                    </Stack>
                </Stack>
            </Slide>
        </Grid>
    );
}