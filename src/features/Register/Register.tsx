import React, {useState} from "react"
import {
    Box,
    Button,
    Checkbox,
    FormControl,
    FormControlLabel,
    Grid, IconButton, Link,
    Slide,
    Stack,
    TextField,
    Typography
} from "@mui/material";
import FacebookIcon from "@mui/icons-material/Facebook";
import {WelcomeSvgComponent} from "../../images/component/WelcomeSvgComponent";
import {Facebook, Google} from "@mui/icons-material";
import LoadingButton from "@mui/lab/LoadingButton";


export function Register() {
    /* START: Variable */
    const [isLoading, setIsLoading] = useState<boolean>(false);
    /* END: Variable */

    /* START: Function */
    const handleRegister = () => {
        setIsLoading(true);
    }
    /* END: Function */

    return (
        <Grid container
              justifyContent={"center"}
              alignItems={"center"}
              p={"25px"}
              height={"100vh"}>
            <Slide direction="left" in={true}>
                <Stack direction={"row"} width={"100%"}>
                    <Stack boxShadow={1}
                           borderRadius={5}
                           alignItems={"center"}
                           justifyContent={"center"}
                           width={"50%"}>
                        <Typography variant={"h3"} fontWeight={"500"}>
                            Hi, Welcome to Zalo Aka!
                        </Typography>
                        <WelcomeSvgComponent/>
                    </Stack>
                    <Stack width={"50%"}
                           alignItems={"center"}
                           justifyContent={"center"}
                           p={"50px"}>
                        <Stack spacing={3} width={"50%"}>
                            {/* START: Title */}
                            <Typography variant={"h3"} fontWeight={"500"}>
                                Register to Zalo Aka
                            </Typography>
                            {/* END: Title */}

                            {/* START: Field Username - Password */}
                            <Box>
                                <Stack spacing={3}>
                                    <Stack direction={"row"}
                                           alignItems={"center"}
                                           justifyContent={"space-between"}>
                                        <TextField label={"First Name"}/>
                                        <TextField label={"Last Name"}/>
                                    </Stack>
                                    <TextField label={"Email address"}/>
                                    <TextField label="Password"
                                               type="password"/>
                                </Stack>
                            </Box>
                            {/* END: Field Username - Password */}

                            {/* START: Login Button */}
                            <Stack direction="row" spacing={2}>
                                <LoadingButton variant={"contained"}
                                               loading={isLoading}
                                               fullWidth
                                               size={"large"}
                                               onClick={() => handleRegister()}>Register</LoadingButton>
                            </Stack>
                            {/* END: Login Button */}

                            {/* START: Register */}
                            <Stack direction="row">
                                <Typography variant={"body1"}>
                                    Already have an account? <Link href={"/login"} underline={"hover"} fontWeight={"500"}>Login</Link>
                                </Typography>
                            </Stack>
                            {/* END: Register */}

                        </Stack>
                    </Stack>
                </Stack>
            </Slide>
        </Grid>
    )
}