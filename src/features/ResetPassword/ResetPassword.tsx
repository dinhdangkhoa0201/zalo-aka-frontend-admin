import React, {useState} from "react";
import {Button, Grid, Stack, TextField, Typography} from "@mui/material";
import LoadingButton from "@mui/lab/LoadingButton";
import {MailSentSvgComponent} from "../../images/component/MailSentSvgComponent";

export function ResetPassword() {
    /* START: Variable */
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [isSent, setIsSent] = useState<boolean>(false);
    /* END: Variable */

    /* START: Function */
    const handleResetPassword = () => {
        setIsLoading(true);
        setIsSent(true);
    }
    /* END: Function */

    return (
        <Grid container
              width={"100%"}
              height={"100vh"}
              alignItems={"center"}
              justifyContent={"center"}>
            <Stack width={"25%"}
                   sx={{display: isSent ? "none" : "flex"}}
                   spacing={3}>
                <Typography variant={"h4"} fontWeight={"500"}>
                    Forgot your password?
                </Typography>
                <Typography variant={"subtitle1"}>
                    Please enter the email address associated with your account and We will email you a link to reset
                    your password.
                </Typography>
                <Stack>
                    <TextField label={"Email address"}/>
                </Stack>
                <Stack spacing={2}>
                    <LoadingButton variant={"contained"}
                                   loading={isLoading}
                                   fullWidth
                                   size={"large"}
                                   onClick={() => handleResetPassword()}>
                        Reset Password
                    </LoadingButton>
                    <Button fullWidth
                            href={"/login"}
                            size={"large"}>
                            Cancel
                    </Button>
                </Stack>
            </Stack>

            <Stack width={"25%"}
                   alignItems={"center"}
                   sx={{display: isSent ? "flex" : "none"}}
                   spacing={2}>
                <MailSentSvgComponent/>
                <Typography variant={"h4"} fontWeight={"500"} alignItems={"center"}>
                    Request sent successfully
                </Typography>
                <Typography variant={"body1"} align={"center"}>
                    <Typography>We have sent a confirmation email to <Typography fontWeight={"500"}>khoacyruss@gmail.com</Typography></Typography>
                    <Typography>Please check your email.</Typography>
                </Typography>
                <Stack>
                    <Button href={"/login"}
                            variant="contained"
                            sx={{paddingLeft: 2.5, paddingRight: 2.5}}
                            size={"large"}>
                        Back
                    </Button>
                </Stack>
            </Stack>
        </Grid>
    )
}