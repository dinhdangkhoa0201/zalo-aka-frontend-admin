import React from "react";
import {Link, Typography} from "@mui/material";

export function Dashboard() {
    return (
        <div>
            <Typography variant={"h1"}>
                Dashboard Page
            </Typography>
            <Link href={"/login"} underline={"hover"}>Login</Link>
            <br/>
            <Link href={"/register"} underline={"hover"}>Register</Link>
            <br/>
            <Link href={"/resetPassword"} underline={"hover"}>Reset Password</Link>
        </div>
    )
}