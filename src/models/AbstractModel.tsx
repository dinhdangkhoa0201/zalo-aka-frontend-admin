export interface AbstractModel {
    id: bigint,
    activeFlag: ActiveFlag,
    usableFlag: UsableFlag,
    createBy: string,
    createDate: Date,
    updateBy: string,
    updateDate: Date,
}