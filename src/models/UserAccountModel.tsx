import {AbstractModel} from "./AbstractModel";

export interface UserAccountModel extends AbstractModel {
    email: string,
    password: string
}