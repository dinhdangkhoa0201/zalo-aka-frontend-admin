import {AbstractModel} from "./AbstractModel";

export interface UserProfileModel extends AbstractModel {
    firstName: string,
    lastName: string,
    gender: GenderFlag | null,
    street: string | null,
    distinct: string | null,
    city: string | null,
    provide: string | null
}