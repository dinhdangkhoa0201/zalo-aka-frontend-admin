import { AbstractModel } from "./AbstractModel";

export interface SystemConstantModel extends AbstractModel {
    code: string,
    message: string,
    desc: string | null
}