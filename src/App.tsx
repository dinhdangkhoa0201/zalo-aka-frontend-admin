import React from 'react';
import './App.css';
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import {Login} from "./features/LogIn/Login";
import {Register} from "./features/Register/Register";
import {Dashboard} from "./features/Dashboard/Dashboard";
import {ResetPassword} from "./features/ResetPassword/ResetPassword";

function App() {
    return (
        <Router>
            <Routes>
                <Route path={"/"} element={<Dashboard/>}/>
                <Route path={"/login"} element={<Login/>}/>
                <Route path={"/register"} element={<Register/>}/>
                <Route path={"/resetPassword"} element={<ResetPassword/>}/>
            </Routes>
        </Router>
    );
}

export default App;
