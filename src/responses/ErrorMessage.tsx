export interface ErrorMessage {
    status: number
    timestamp: Date,
    message: string,
    errors: Array<string>,
    path: string
}