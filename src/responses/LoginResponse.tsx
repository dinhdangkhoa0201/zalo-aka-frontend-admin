import {UserAccountModel} from "../models/UserAccountModel";

export interface LoginResponse {
    userAccount: UserAccountModel,
    refreshToken: string
    token: string,
    prefixToken: string
}