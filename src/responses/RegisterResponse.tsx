import {UserAccountModel} from "../models/UserAccountModel";

export interface RegisterResponse {
    userAccount: UserAccountModel,
    token: string
}