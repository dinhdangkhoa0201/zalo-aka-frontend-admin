export interface DataTableResponse<T> {
    objects: Array<T>,
    activePage: number,
    pageSize: number,
    total: number
}