export const apiConstant = {
    host: "http://localhost",
    port: "8002",
    requestMapping: "/api/v1",
    authAPIUrl: {
        login: "/auth/login",
        register: "/auth/register",
        verify: "/auth/verify"
    },
    systemConstantAPIUrl: {
        save: "/systemConstant",
        update: "/systemConstant",
        findByCriteria: "/systemConstant/findByCriteria"
    }
}