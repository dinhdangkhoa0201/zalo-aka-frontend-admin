export interface RegisterRequest {
    lastName: string,
    firstName: string,
    email: string,
    password: string
}