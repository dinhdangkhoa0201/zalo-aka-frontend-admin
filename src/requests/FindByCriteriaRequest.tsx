export interface FindByCriteriaRequest {
    criteria: Map<string, object>,
    orderBy: string[],
    activePage: number,
    pageSize: number
}