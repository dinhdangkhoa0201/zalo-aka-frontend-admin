import httpConfig from "./index"
import {LoginResponse} from "../responses/LoginResponse";
import {LoginRequest} from "../requests/LoginRequest";
import {apiConstant} from "../constant/APIConstant";
import {RegisterResponse} from "../responses/RegisterResponse";
import {RegisterRequest} from "../requests/RegisterRequest";
import {UserProfileModel} from "../models/UserProfileModel";
import {VerifyTokenRequest} from "../requests/VerifyTokenRequest";

const login = async (request: LoginRequest): Promise<LoginResponse> => {
    return await httpConfig.post(apiConstant.authAPIUrl.login, {
        email: request.email,
        password: request.password
    });
}

const register = async (request: RegisterRequest): Promise<RegisterResponse> => {
    return await httpConfig.post(apiConstant.authAPIUrl.register, {
        firstName: request.firstName,
        lastName: request.lastName,
        email: request.email,
        password: request.password
    })
}

const verify = async (request: VerifyTokenRequest): Promise<UserProfileModel> => {
    return await httpConfig.post(apiConstant.authAPIUrl.verify, {
        token: request.token
    })
}

export const authAPI = {
    login,
    register,
    verify
}