import {SystemConstantModel} from "../models/SystemConstantModel"
import httpConfig from "./index"
import {FindByCriteriaRequest} from "../requests/FindByCriteriaRequest";
import {DataTableResponse} from "../responses/DataTableResponse";
import {apiConstant} from "../constant/APIConstant";

const save = async (model: SystemConstantModel): Promise<SystemConstantModel> => {
    return await httpConfig.post(apiConstant.systemConstantAPIUrl.save, {
        code: model.code,
        message: model.message,
        desc: model.desc
    })
}

const update = async (id: bigint, model: SystemConstantModel): Promise<SystemConstantModel> => {
    return await httpConfig.post(`${apiConstant.systemConstantAPIUrl.update}/${id}`, {
        id: model.id,
        code: model.code,
        message: model.message,
        desc: model.desc,
        activeFlag: model.activeFlag,
        usableFlag: model.usableFlag,
        createBy: model.createBy,
        createDate: model.createDate,
        updateBy: model.updateBy,
        updateDate: model.updateDate
    })
}

const findByCriteria = async (request: FindByCriteriaRequest): Promise<DataTableResponse<SystemConstantModel>> => {
    return await httpConfig.post(apiConstant.systemConstantAPIUrl.findByCriteria, {
        criteria: request.criteria,
        orderBy: request.orderBy,
        activePage: request.activePage,
        pageSize: request.pageSize
    })
}

export const systemConstantAPI = {
    save,
    update,
    findByCriteria
}